msgid ""
msgstr ""
"Project-Id-Version: Widelands\n"
"Report-Msgid-Bugs-To: https://www.widelands.org/wiki/ReportingBugs/\n"
"POT-Creation-Date: 2025-01-02 02:52+0000\n"
"PO-Revision-Date: 2015-02-03 14:49+0000\n"
"Last-Translator: tamanegi, 2018\n"
"Language-Team: Japanese (http://app.transifex.com/widelands/widelands/"
"language/ja/)\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Translate Toolkit 3.15.0\n"

# (pofilter) numbers: Different numbers
#: ../../../../data/campaigns/tutorial04_economy.wmf/scripting/texts.lua:87
msgid ""
"‘2/4’ below your sentry: For military buildings, the stationed soldiers are "
"shown instead of a productivity. You want to have four soldiers in your "
"sentries, but only two soldiers are stationed in this kind of building. This"
" leaves two vacant positions – we really need more soldiers."
msgstr "詰所の下の「2/4」: 軍事建物では、生産性の代わりに駐留する兵士数が表示されます。詰所全体で合計で４名置いておきたいところですが、現在は合計で２名しかいないという意味になります。つまりは2人分の空きがあるということで ── 我々にはもっと兵士が必要だということです。"

# (pofilter) numbers: Different numbers
#: ../../../../data/campaigns/tutorial04_economy.wmf/scripting/texts.lua:542
msgid ""
"Bring all of the 20 marble columns to the warehouse near the front line."
msgstr "全ての石柱を前線近くにある倉庫へ輸送しましょう"
