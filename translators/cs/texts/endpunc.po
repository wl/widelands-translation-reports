msgid ""
msgstr ""
"Project-Id-Version: Widelands\n"
"Report-Msgid-Bugs-To: https://www.widelands.org/wiki/ReportingBugs/\n"
"POT-Creation-Date: 2025-03-01 03:04+0000\n"
"PO-Revision-Date: 2015-02-03 14:50+0000\n"
"Last-Translator: Ondřej Itřech, 2020-2021,2025\n"
"Language-Team: Czech (http://app.transifex.com/widelands/widelands/language/"
"cs/)\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"
"X-Generator: Translate Toolkit 3.15.0\n"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/txts/README.lua:30
msgid ""
"Which version of Widelands you are running (i.e. either the build number or "
"the revision number if you are running a development version or a daily "
"build.)"
msgstr "Kterou verzi Widelands používáš (tj. buď pořadové číslo aktualizace nebo ověřovací ID, pokud používáš vývojovou verzi nebo denní aktualizaci)."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/txts/help/multiplayer_help.lua:22
msgid ""
"The latter three can only be set by the hosting client by left-clicking the "
"‘type’ button of a player. Hosting players can also set the initialization "
"of each player (the set of buildings, wares and workers the player starts "
"with) and the tribe and team for computer players."
msgstr "Jednu z posledních tří možností může nastavit pouze hostitel, kliknutím na tlačítko \"Typ\" u každého z hráčů. Hostitel může také nastavovat startovní podmínky u všech hráčů (budovy, zboží a pracovníky, které hráči na začátku hry dostanou) a u počítačových hráčů navíc i kmen a tým ."
