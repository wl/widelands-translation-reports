msgid ""
msgstr ""
"Project-Id-Version: Widelands svnVERSION\n"
"Report-Msgid-Bugs-To: https://www.widelands.org/wiki/ReportingBugs/\n"
"POT-Creation-Date: 2025-01-02 02:52+0000\n"
"PO-Revision-Date: 2017-12-27 17:17+0000\n"
"Last-Translator: Renaud Bouchard, 2024\n"
"Language-Team: French (https://app.transifex.com/widelands/teams/35159/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % "
"1000000 == 0 ? 1 : 2;\n"
"X-Generator: Translate Toolkit 3.15.0\n"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:190
msgid "Build a training camp and enhance the arena"
msgstr "Construisez un camp d’entraînement et améliorez l'arène."

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Lutius
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:248
msgid ""
"Oh no! Amalea is right. In fact, I can’t see any productivity at all. And "
"the road network seems to be completely in shambles as well. Who might be "
"responsible for this chaos?"
msgstr ""
"Oh non ! Amalea à raison. En fait, je ne vois plus aucune productivité du "
"tout.. Et le réseau routier semble aussi complètement en désordre. Qui "
"pourrait être responsable de ce chaos ?"

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Lutius
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:256
msgid "Ave! Who are you and what happened to our beautiful land?"
msgstr "Avé ! Qui êtes vous et qu'est il arrivé à notre beau royaume ?"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:276
msgid "A high Fremil official is welcoming you…"
msgstr "Un haut représentant de Fremil vous accueille..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:286
msgid "The official sighs deeply…"
msgstr "Le représentant soupire profondément..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:301
msgid "Amalea is doubtful…"
msgstr "Amalea hésite..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:312
msgid "Amalea is nodding thoughtfully…"
msgstr "Amalea acquiesce pensivement ..."

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Amalea
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:317
msgid ""
"So, first of all we need some building materials to start correcting the "
"mistakes made by the infamous secretary. I think we should try dismantling "
"unproductive small buildings to recover some building materials from them "
"and collect them in our headquarters. As far as I can see now, the "
"fishermen’s houses and the quarries don’t have any resources near them. All "
"of the lumberjacks’ houses and the wells seem also to be inefficient or worn"
" out."
msgstr "Donc, tout d'abord nous avons besoin de materi"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:331
msgid "Amalea recommends…"
msgstr "Amalea recommande..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:366
msgid "Amalea is giving a deep sigh…"
msgstr "Amalea pousse un profond soupir"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:381
msgid "Amalea smiles for the first time in weeks…"
msgstr "Amalea sourit pour la première fois depuis des semaines..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:403
msgid "Amalea is nodding her head…"
msgstr "Amalea hoche la tête..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:415
msgid "Amalea is getting fed up with all the problems in this economy…"
msgstr "Amalea en a assez de tous ces problèmes économiques..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:434
msgid "Amalea is laughing sarcastically…"
msgstr "Amalea rie de manière sarcastique"

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Amalea
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:439
msgid "I am really very curious about what will go wrong next!"
msgstr "Je suis très curieux de voir ce qui va aller de travers ensuite !"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:445
msgid "Amalea is getting used to bad news…"
msgstr "Amalea est habitué aux mauvaises nouvelles"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:461
msgid "Amalea comes in…"
msgstr "Amalea entre..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:473
msgid "Amalea is really sad…"
msgstr "Amalea est vraiment triste..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:484
msgid "Amalea claps her hands…"
msgstr "Amalea tape dans ses mains"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:495
msgid "Amalea is satisfied with the progress…"
msgstr "Amalea est satisfaite des progrès accompli..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:511
msgid "Amalea is providing economic advice…"
msgstr "Amalea fournit des conseils économiques..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:524
msgid "Amalea is celebrating success…"
msgstr "Amalea célèbre la réussite..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:532
msgid "Amalea is sorrowful…"
msgstr "Amalea est emplie de tristesse"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:540
msgid "Amalea is celebrating a happy event…"
msgstr "Amalea célèbre un heureux évènement"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:548
msgid "Amalea reminds Lutius of the farms…"
msgstr "Amalea fait un rappel à Lutius concernant les fermes..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:561
msgid "Amalea restricts the building possibilities…"
msgstr "Amalea restreint les possibilités de constructions..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:572
msgid "Amalea reminds Lutius of the scrambled road network…"
msgstr ""
"Amalea fait un rappel à Lutius concernant la saturation du réseau routier..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:582
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:601
msgid "Defeated!"
msgstr "Vaincu !"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:583
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:602
msgid "Amalea reports our defeat…"
msgstr "Amalea rapporte notre défaite..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:591
msgid "Amalea reports our headquarters lost…"
msgstr "Amalea rapporte la perte de nos quartiers-généraux..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:610
msgid "Amalea is very busy…"
msgstr "Amalea est très occupée..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:623
msgid "Saledus looks very relaxed…"
msgstr "Saledus semble très détendu..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:631
msgid "Saledus raises his voice…"
msgstr "Saledus hausse la voix..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:644
msgid "Saledus is cheering proudly…"
msgstr "Saledus applaudit fièrement..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:652
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:660
msgid "Saledus asserts his point…"
msgstr "Saledus affirme son opinion..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:669
msgid "Saledus asks for a stronger army…"
msgstr "Saledus demande une armée plus puissante..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:685
msgid "Saledus still has security concerns…"
msgstr "Saledus a toujours des problèmes de sécurité..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:697
msgid "Saledus is happy…"
msgstr "Saledus est content..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:709
msgid "Saledus is in a good mood…"
msgstr "Saledus est de bonne humeur..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:723
msgid "Saledus is excited…"
msgstr "Saledus est exalté..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:727
msgid ""
"Let’s finish them off and regain control over our lands. They shall regret "
"deeply that they ever came!"
msgstr ""
"Finissons-les et reprenons le contrôle de nos terres. Ils regretteront "
"profondément d'être venus !"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:735
msgid "Saledus is cheering loudly…"
msgstr "Saledus applaudit bruyamment..."

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Saledus
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:737
msgid ""
"Sire, finally we have defeated the Barbarians. We have expelled even the "
"last of them. May they never come back!"
msgstr ""
"Sire, nous avons enfin vaincu les Barbares. Nous avons expulsé le dernier "
"d'entre eux. Qu'ils ne reviennent jamais !"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:745
msgid "Saledus is alerted…"
msgstr "Saledus est alerté..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:749
msgid ""
"We have to finish them off and regain control over our lands. They shall "
"regret deeply that they ever came!"
msgstr ""
"Nous devons les achever et reprendre le contrôle de nos terres. Ils "
"regretteront profondément d'être venus !"

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:761
msgid "Julia is demanding a sacrifice for Vesta…"
msgstr "Julia demande un sacrifice pour Vesta..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:769
msgid "Vesta is blessing us…"
msgstr "La bénédiction de Vesta est sur nous..."

# (pofilter) endpunc: Different punctuation at the end
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:777
msgid "Vesta is cursing us…"
msgstr "La malédiction de Vesta est sur nous..."

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Julia - priestess of the goddess Vesta
#: ../../../../data/campaigns/emp04.wmf/scripting/texts.lua:779
msgid ""
"Damn you Lutius for killing peaceful servants of the goddess Vesta! May your"
" life and your land be cursed and may the wrath of the goddess scourge your "
"family from the face of the earth!"
msgstr ""
"Maudit soit Lutius pour avoir tué les paisibles serviteurs de la déesse "
"Vesta ! Que votre vie et votre terre soient maudites et que la colère de la "
"déesse éradique votre famille de la surface de la terre !"

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Purpose helptext for an Empire training site: Training Camp,
#. part 1
#: ../../../../data/campaigns/emp04.wmf/scripting/tribes/init.lua:138
msgctxt "empire_building"
msgid "Trains soldiers in ‘Attack’ and in ‘Health’."
msgstr "Entraine les soldats en 'Attaque' et en 'Santé'"

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Empire training site tooltip when it has no soldiers assigned
#: ../../../../data/campaigns/emp04.wmf/scripting/tribes/trainingcamp1/init.lua:177
msgctxt "empire_building"
msgid "No soldier to train!"
msgstr "Pas de soldat à entraîner !"

# (pofilter) endpunc: Different punctuation at the end
#. TRANSLATORS: Empire training site tooltip when none of the present soldiers
#. match the current training program
#: ../../../../data/campaigns/emp04.wmf/scripting/tribes/trainingcamp1/init.lua:179
msgctxt "empire_building"
msgid "No soldier found for this training level!"
msgstr "Aucun soldat trouvé pour ce niveau d’entraînement !"
