msgid ""
msgstr ""
"Project-Id-Version: Widelands\n"
"Report-Msgid-Bugs-To: https://www.widelands.org/wiki/ReportingBugs/\n"
"POT-Creation-Date: 2025-01-02 02:52+0000\n"
"PO-Revision-Date: 2015-02-03 14:53+0000\n"
"Last-Translator: Juanjo, 2015-2024\n"
"Language-Team: Catalan (http://app.transifex.com/widelands/widelands/"
"language/ca/)\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Translate Toolkit 3.15.0\n"

# (pofilter) numbers: Different numbers
#. TRANSLATORS: Helptext for a Barbarian ware: Strong Beer
#: ../../../../data/tribes/initialization/barbarians/units.lua:224
msgctxt "barbarians_ware"
msgid ""
"Only this beer is acceptable for the soldiers in a battle arena. Some say "
"that the whole power of the Barbarians lies in this ale. It helps to train "
"the soldiers’ evade level from 0 to 1 to 2. Strong beer is also used in big "
"inns to prepare meals."
msgstr "Els guerrers a les palestres d’entrenament només volen cervesa negra. Alguns diuen que la força dels bàrbars ve d’aquesta beguda. Ajuda a entrenar el nivell d’evasió dels soldats. També s’utilitza cervesa negra en hostals grans per preparar àpats."

# (pofilter) numbers: Different numbers
#. TRANSLATORS: Purpose helptext for a Frisian training site: Training Camp
#: ../../../../data/tribes/initialization/frisians/units.lua:2131
msgctxt "frisians_building"
msgid ""
"Trains soldiers in Attack up to level 3 as well as in Defense and Health to "
"level 1. Equips the soldiers with all necessary weapons and armor parts."
msgstr "Entrena els soldats en atac fins al tercer nivell i en defensa i salut fins al nivell 1. Equipa els soldats amb les armes i armadures que necessitin."
